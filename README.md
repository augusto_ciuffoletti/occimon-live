# Demo of an OCCI monitoring implementation #

* This is a dockerized demo based on Socket/RMI
* You should install docker and start the daemon
* Add you user to the "docker" group and log in. Alternatively (please don't) run the script as root.
* Run the script.sh and follow on-screen instructions
* See also https://hub.docker.com/r/mastrogeppetto/occimon-live/
* Author: Augusto Ciuffoletti (augusto.ciuffoletti@di.unipi.it)


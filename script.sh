#!/bin/bash

SERVER="docker run -ti --expose=6789 --hostname=server --name=server mastrogeppetto/occimon-live:v0.4 /usr/local/occimon-live/bin/httpServer.py"

DASHBOARD="docker run -ti --hostname=monitor --name=dashboard --expose=8888 mastrogeppetto/occimon-live:v0.4 nc -ul 8888"

COMPUTE="docker run -ti --hostname=pc1 --name=compute --expose=12312 --link=server:server mastrogeppetto/occimon-live:v0.4 /usr/local/occimon-live/bin/probe.sh urn:uuid:c2222"

SENSOR="docker run -ti --hostname=sensor --name=sensor --link=server:server --link=compute:compute --link=dashboard:dashboard mastrogeppetto/occimon-live:v0.4 /usr/local/occimon-live/bin/sensor.sh urn:uuid:s1111"

delay=10 # delay between each step

echo -e "\nPlease inspect the script to see the docker commands that are issued"
echo "This script is deliberately delayed"

echo -e "\nPlease move this terminale window at the bottom of your screen"
echo "  and wait for the demo to start"
sleep 5

xterm -geometry "80x24+0+50" -T "web server" -e "$SERVER" &
sleep 1
echo "***"
echo "The web server has been successfully instantiated and is now running"
echo "It will provide system components with OCCI descriptions for configuration"
echo -e "The demo will automatically proceed in $delay seconds\n"
sleep $delay

xterm -geometry "80x24+200+100" -T "dashboard" -e "$DASHBOARD" &
sleep 1
echo "***"
echo "The dashboard has been successfully instantiated and is now running"
echo "It will receive and show the monitoring results from the sensor"
echo -e "The demo will automatically proceed in $delay seconds\n"
sleep $delay

xterm -geometry "80x24+400+150" -T "compute" -e "$COMPUTE" &
sleep 1
echo "***"
echo "The compute resource has been successfully instantiated and is now running"
echo "The resource is ready to start a collector endpoint thread when requested by the sensor"
echo -e "The demo will automatically proceed in $delay seconds\n"
sleep $delay

xterm -geometry "80x24+600+200" -T "sensor" -e "$SENSOR" &
sleep 1
echo "***"
echo "The sensor resource has been successfully instantiated and is now running"
echo "The sensor first GETs the local configuration and configure the aggregator and publisher mixins"
echo "The sensor then GETs the collector configuration and configure the remote endpoint"
echo -e "The monitoring data are read from the collector, and either logged locally or delivered to the dashboard\n"

sleep 1
echo "***"
echo " Demo is now up and running: inspect the components with \"docker exec\". "
echo " For instance:"
echo " $ docker exec -it server /bin/bash"
echo " to log into the web server and see the OCCI resource descriptions"
echo " Press return to terminate the demo"
echo -n " --> "

read

echo -n "terminate "
docker rm -f server
sleep 1
echo -n "terminate "
docker rm -f dashboard
sleep 1
echo -n "terminate "
docker rm -f compute
sleep 1
echo -n "terminate "
docker rm -f sensor
